// JavaScript Document
$(document).ready(function() {

/*
 *Criado por: Leonardo Dessandes
 */
		
    $('.mancha,.lista_01,.lista_02,.lista_03,.lista_04,.lista_05,.lista_06,.lista_07,.lista_08,.resp_lista_01,.resp_lista_02,.resp_lista_03,.resp_lista_04,.resp_lista_05,.resp_lista_06,.resp_lista_07,.resp_lista_08').hide();
				
	$('.mancha').delay(1000).fadeIn();
	$('.lista_01').delay(2000).fadeIn();
	$('.resp_lista_01').delay(2500).fadeIn();
	$('.lista_02').delay(3500).fadeIn();
	$('.resp_lista_02').delay(4000).fadeIn();
	$('.lista_03').delay(5000).fadeIn();
	$('.resp_lista_03').delay(5500).fadeIn();
	$('.lista_04').delay(6500).fadeIn();
	$('.resp_lista_04').delay(7000).fadeIn();
	$('.lista_05').delay(8000).fadeIn();
	$('.resp_lista_05').delay(8500).fadeIn();
	$('.lista_06').delay(9500).fadeIn();
	$('.resp_lista_06').delay(10000).fadeIn();
	$('.lista_07').delay(11000).fadeIn();
	$('.resp_lista_07').delay(11500).fadeIn();
	$('.lista_08').delay(12500).fadeIn();
	$('.resp_lista_08').delay(13000).fadeIn();

});

$(document).ready(function() {
    $('.texto_01_,.texto_02_,.texto_03_,.texto_04,.texto_05_,.texto_06_').hide();
	
	$('.texto_01_').delay(1300).fadeIn();
	$('.texto_02_').delay(2000).fadeIn();
	$('.texto_03_').delay(2500).fadeIn();
	$('.texto_04').delay(3000).fadeIn();
	$('.texto_05_').delay(3500).fadeIn();
	$('.texto_06_').delay(4000).fadeIn();
	
	$('.texto_02_').click(
		function(){
		$('.flutuar-01').fadeIn();
	});
	
	$('.texto_03_').click(
		function(){
		$('.flutuar-02').fadeIn();
	});
	
	$('.texto_04').click(
		function(){
		$('.flutuar-03').fadeIn();
	});
	
	$('.texto_05_').click(
		function(){
		$('.flutuar-04').fadeIn();
	});
	
	$('.texto_06_').click(
		function(){
		$('.flutuar-05').fadeIn();
	});
	
});

$(document).ready(function() {
    $('.flutuar-01,.flutuar-02,.flutuar-03,.flutuar-04,.flutuar-05').hide();
	
	$('.feharmeu').click(
		function(){
		$('.flutuar-01,.flutuar-02,.flutuar-03,.flutuar-04,.flutuar-05').fadeOut();
	});
});

$(document).ready(function() {
    $('.text_circ_01_,.text_circ_02,.text_circ_03,.text_circ_04').hide();
	
	$('.circ_01_').click(
		function(){
		$('.text_circ_01_').fadeIn(300);
	});
	
	$('.circ_02_').click(
		function(){
		$('.text_circ_02').fadeIn(300);
	});
	
	$('.circ_03_').click(
		function(){
		$('.text_circ_03').fadeIn(300);
	});
	
	$('.circ_04_').click(
		function(){
		$('.text_circ_04').fadeIn(300);
	});
	
	$('.feharmeu2').click(
		function(){
		$('.text_circ_01_,.text_circ_02,.text_circ_03,.text_circ_04').fadeOut();
	});
});

$(document).ready(function() {
    $('.banner_piramide01, .banner_piramide02, .banner_piramide03, .banner_piramide04, .banner_piramide05, .banner_piramide06').hide();
	
	$('.piramide01_tijolo').click(
		function(){
		$('.banner_piramide01').fadeIn(300);
	});
	
	$('.piramide02_tijolo').click(
		function(){
		$('.banner_piramide02').fadeIn(300);
	});
	
	$('.piramide03_tijolo').click(
		function(){
		$('.banner_piramide03').fadeIn(300);
	});
	
	$('.piramide04_tijolo').click(
		function(){
		$('.banner_piramide04').fadeIn(300);
	});
	
	$('.piramide05_tijolo').click(
		function(){
		$('.banner_piramide05').fadeIn(300);
	});
	
	$('.piramide06_tijolo').click(
		function(){
		$('.banner_piramide06').fadeIn(300);
	});
	
	$('.fechar_texto').click(
		function(){
		$('.banner_piramide01, .banner_piramide02, .banner_piramide03, .banner_piramide04, .banner_piramide05, .banner_piramide06').fadeOut(400);
	});
	
});

$(document).ready(function() {
	$('.button_01_02, .button_02_02, .button_03_02, .button_04_02, .button_05_02').hide();
		
		//botão 01
		$('.button_01_01').mouseover(
		function(){
     		$('.button_01_02').fadeIn(400);
		});
	
		$('.button_01_01').mouseout(
		function(){
     		$('.button_01_02').fadeOut(300);
		});
		
		//botão 02
		$('.button_02_01').mouseover(
		function(){
     		$('.button_02_02').fadeIn(400);
		});
	
		$('.button_02_01').mouseout(
		function(){
     		$('.button_02_02').fadeOut(300);
		});
		
		//botão 03
		$('.button_03_01').mouseover(
		function(){
     		$('.button_03_02').fadeIn(400);
		});
	
		$('.button_03_01').mouseout(
		function(){
     		$('.button_03_02').fadeOut(300);
		});
		
		//botão 04
		$('.button_04_01').mouseover(
		function(){
     		$('.button_04_02').fadeIn(400);
		});
	
		$('.button_04_01').mouseout(
		function(){
     		$('.button_04_02').fadeOut(300);
		});
		
		//botão 05
		$('.button_05_01').mouseover(
		function(){
     		$('.button_05_02').fadeIn(400);
		});
	
		$('.button_05_01').mouseout(
		function(){
     		$('.button_05_02').fadeOut(300);
		});
			
});

$(document).ready(function() {
    $('.banner_circulo01, .banner_circulo02, .banner_circulo03, .banner_circulo04, .banner_circulo05').hide();
	
	$('.button_01_02').click(
		function(){
		$('.banner_circulo01').fadeIn(300);
	});
	
	$('.button_01_01').click(
		function(){
		$('.banner_circulo01').fadeIn(300);
	});
	
	$('.button_02_02').click(
		function(){
		$('.banner_circulo02').fadeIn(300);
	});
	
	$('.button_02_01').click(
		function(){
		$('.banner_circulo02').fadeIn(300);
	});
	
	$('.button_03_02').click(
		function(){
		$('.banner_circulo03').fadeIn(300);
	});
	
	$('.button_03_01').click(
		function(){
		$('.banner_circulo03').fadeIn(300);
	});
	
	$('.button_04_02').click(
		function(){
		$('.banner_circulo04').fadeIn(300);
	});
	
	$('.button_04_01').click(
		function(){
		$('.banner_circulo04').fadeIn(300);
	});
	
	$('.button_05_02').click(
		function(){
		$('.banner_circulo05').fadeIn(300);
	});
	
	$('.button_05_01').click(
		function(){
		$('.banner_circulo05').fadeIn(300);
	});
	
	$('.fechar_texto').click(
		function(){
		$('.banner_circulo01, .banner_circulo02, .banner_circulo03, .banner_circulo04, .banner_circulo05').fadeOut(400);
	});
	
});